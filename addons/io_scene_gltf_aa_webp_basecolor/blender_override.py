import bpy
import typing
import os

from io_scene_gltf2.blender.exp.gltf2_blender_gather_texture_info import (
    __filter_texture_info,
    __gather_texture_transform_and_tex_coord,
    __gather_extensions as gather_texture_info_extensions,
    __gather_extras as gather_texture_info_extras,
)
from io_scene_gltf2.io.com import gltf2_io
from io_scene_gltf2.blender.exp.gltf2_blender_gather_texture import (
    __filter_texture,
    __gather_extensions as gather_texture_extensions,
    __gather_extras as gather_texture_extras,
    __gather_name as gather_texture_name,
    __gather_sampler,
)
from io_scene_gltf2.blender.exp.gltf2_blender_gather_image import (
    __filter_image,
    __gather_extensions as gather_image_extensions,
    __gather_extras as gather_image_extras,
    __gather_name as gather_image_name,
    __gather_buffer_view,
    __make_image,
    __get_tex_from_socket,
)
from io_scene_gltf2.io.exp import gltf2_io_binary_data


def get_image_data(sockets, export_settings):
    results = [__get_tex_from_socket(socket, export_settings) for socket in sockets]

    return result


def gather_mime_type(blender_shader_sockets, image_data, export_settings):
    return "video/webp"


def gather_image(
    blender_shader_sockets: typing.Tuple[bpy.types.NodeSocket], export_settings
):
    if not __filter_image(blender_shader_sockets, export_settings):
        return None, None

    results = [
        __get_tex_from_socket(socket, export_settings)
        for socket in blender_shader_sockets
    ]
    if results[0] is None:
        return None, None

    image = results[0].shader_node.image
    if not image.filepath.endswith(".webp"):
        return None, None

    mime_type = "video/webp"
    name = "flippi"

    factor = None

    data = None
    if image.source == "FILE" and not image.is_dirty:
        if image.packed_file is not None:
            data = image.packed_file.data
        else:
            src_path = bpy.path.abspath(image.filepath_raw)
            if os.path.isfile(src_path):
                with open(src_path, "rb") as f:
                    data = f.read()

    if data is None:
        return None, None

    buffer_view = gltf2_io_binary_data.BinaryData(data=data)

    image = __make_image(
        buffer_view,
        gather_image_extensions(blender_shader_sockets, export_settings),
        gather_image_extras(blender_shader_sockets, export_settings),
        mime_type,
        name,
        None,
        export_settings,
    )

    return image, factor


def gather_texture(
    blender_shader_sockets: typing.Tuple[bpy.types.NodeSocket], export_settings
):
    """
    Gather texture sampling information and image channels from a blender shader texture attached to a shader socket.

    :param blender_shader_sockets: The sockets of the material which should contribute to the texture
    :param export_settings: configuration of the export
    :return: a glTF 2.0 texture with sampler and source embedded (will be converted to references by the exporter)
    """

    if not __filter_texture(blender_shader_sockets, export_settings):
        return None, None

    source, factor = gather_image(blender_shader_sockets, export_settings)

    texture = gltf2_io.Texture(
        extensions=gather_texture_extensions(blender_shader_sockets, export_settings),
        extras=gather_texture_extras(blender_shader_sockets, export_settings),
        name=gather_texture_name(blender_shader_sockets, export_settings),
        sampler=__gather_sampler(blender_shader_sockets, export_settings),
        source=source,
    )

    # although valid, most viewers can't handle missing source properties
    # This can have None source for "keep original", when original can't be found
    if texture.source is None:
        return None, None

    return texture, factor


def gather_texture_info(
    primary_socket: bpy.types.NodeSocket,
    blender_shader_sockets: typing.Tuple[bpy.types.NodeSocket],
    export_settings,
):
    if not __filter_texture_info(
        primary_socket, blender_shader_sockets, "ALL", export_settings
    ):
        return None, None, None

    (
        tex_transform,
        tex_coord,
        use_active_uvmap,
    ) = __gather_texture_transform_and_tex_coord(primary_socket, export_settings)

    index, factor = gather_texture(blender_shader_sockets, export_settings)

    fields = {
        "extensions": gather_texture_info_extensions(tex_transform, export_settings),
        "extras": gather_texture_info_extras(blender_shader_sockets, export_settings),
        "index": index,
        "tex_coord": tex_coord,
    }

    texture_info = gltf2_io.TextureInfo(**fields)

    if texture_info.index is None:
        return None, None, None

    return texture_info, use_active_uvmap, factor
