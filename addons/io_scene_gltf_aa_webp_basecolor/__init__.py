import bpy
from io_scene_gltf2.blender.exp import (
    gltf2_blender_get,
    gltf2_blender_gather_texture_info,
)
from .blender_override import gather_texture_info

bl_info = {
    "name": "AA WebP BaseColor",
    "category": "Import-Export",
    "version": (1, 0, 0),
    "blender": (3, 5, 0),
    "location": "File > Export > glTF 2.0",
    "description": "Example addon to add a custom extension to an exported glTF file.",
    # Replace with your issue tracker
    "tracker_url": "https://example.org",
    "isDraft": False,
    "developer": "AA",  # Replace this
    "url": "https://www.akronyme-analogiker.jetzt/",  # Replace this
}

# glTF extensions are named following a convention with known prefixes.
# See: https://github.com/KhronosGroup/glTF/tree/master/extensions#about-gltf-extensions
# also: https://github.com/KhronosGroup/glTF/blob/master/extensions/Prefixes.md
glTF_extension_name = "AA_webp_basecolor"

# Support for an extension is "required" if a typical glTF viewer cannot be expected
# to load a given model without understanding the contents of the extension.
# For example, a compression scheme or new image format (with no fallback included)
# would be "required", but physics metadata or app-specific settings could be optional.
extension_is_required = False


class AAWebPBaseColorExtensionProperties(bpy.types.PropertyGroup):
    enabled: bpy.props.BoolProperty(
        name=bl_info["name"],
        description="Include this extension in the exported glTF file.",
        default=True,
    )


def register():
    bpy.utils.register_class(AAWebPBaseColorExtensionProperties)
    bpy.types.Scene.AAWebPBaseColorExtensionProperties = bpy.props.PointerProperty(
        type=AAWebPBaseColorExtensionProperties
    )


def register_panel():
    # Register the panel on demand, we need to be sure to only register it once
    # This is necessary because the panel is a child of the extensions panel,
    # which may not be registered when we try to register this extension
    try:
        bpy.utils.register_class(GLTF_PT_UserExtensionPanel)
    except Exception:
        pass

    # If the glTF exporter is disabled, we need to unregister the extension panel
    # Just return a function to the exporter so it can unregister the panel
    return unregister_panel


def unregister_panel():
    # Since panel is registered on demand, it is possible it is not registered
    try:
        bpy.utils.unregister_class(GLTF_PT_UserExtensionPanel)
    except Exception:
        pass


def unregister():
    unregister_panel()
    bpy.utils.unregister_class(AAWebPBaseColorExtensionProperties)
    del bpy.types.Scene.AAWebPBaseColorExtensionProperties


class GLTF_PT_UserExtensionPanel(bpy.types.Panel):
    bl_space_type = "FILE_BROWSER"
    bl_region_type = "TOOL_PROPS"
    bl_label = "Enabled"
    bl_parent_id = "GLTF_PT_export_user_extensions"
    bl_options = {"DEFAULT_CLOSED"}

    @classmethod
    def poll(cls, context):
        sfile = context.space_data
        operator = sfile.active_operator
        return operator.bl_idname == "EXPORT_SCENE_OT_gltf"

    def draw_header(self, context):
        props = bpy.context.scene.AAWebPBaseColorExtensionProperties
        self.layout.prop(props, "enabled")

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False  # No animation.

        props = bpy.context.scene.AAWebPBaseColorExtensionProperties
        layout.active = props.enabled

        box = layout.box()
        box.label(text=glTF_extension_name)


class glTF2ExportUserExtension:
    def __init__(self):
        # We need to wait until we create the gltf2UserExtension to import the gltf2 modules
        # Otherwise, it may fail because the gltf2 may not be loaded yet
        from io_scene_gltf2.io.com.gltf2_io_extensions import Extension

        self.Extension = Extension
        self.properties = bpy.context.scene.AAWebPBaseColorExtensionProperties

    def __gather_webp_base_color_texture(self, blender_material, export_settings):
        shadow = gltf2_blender_get.get_socket(blender_material, "Base Color")
        export_settings = export_settings | {"gltf_keep_original_textures": True}
        print("GEIL", shadow)
        return gather_texture_info(shadow, (shadow,), export_settings)

    def gather_material_hook(self, gltf2_object, blender_object, export_settings):
        if self.properties.enabled:
            if gltf2_object.extensions is None:
                gltf2_object.extensions = {}

            (
                webp_base_color_texture,
                use_active_uvmap,
                factor,
            ) = self.__gather_webp_base_color_texture(blender_object, export_settings)
            print("EXTRAGEIL", webp_base_color_texture, use_active_uvmap, factor)
            if webp_base_color_texture is not None:
                print("EXTRAGEIL2")
                gltf2_object.extensions[glTF_extension_name] = self.Extension(
                    "AA_webp_basecolor",
                    {"baseColorTexture": webp_base_color_texture},
                    False,
                )
