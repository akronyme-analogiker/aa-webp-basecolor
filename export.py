import bpy
import addon_utils

addon_utils.enable("io_scene_gltf_aa_webp_basecolor", default_set=True)

result = bpy.ops.export_scene.gltf(
    filepath="webp",
    export_format="GLTF_EMBEDDED",
    export_cameras=True,
    export_extras=True,
    export_apply=False,
    export_lights=True,
)
print(result)
